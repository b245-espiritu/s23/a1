console.log('hellow');

class Trainer{
    constructor(name, age, pokemons, hoennFriends, kantoFriends){
        this.name = name;
        this.age = age;
        this.pokemons = pokemons.map(item=> item.name);
        this.friends = {hoenn:hoennFriends, kanto:kantoFriends}
       

        //method

        this.talk = function(pokemon){
            console.log(`${pokemon.name}! I choose you`);
        }
    }
}




class Pokemon{
    constructor(name, level){
        this.name = name;
        this.level = level;
        this.health = level * 2;
        this.attack = level;

        // method
        this.faint = function(targetPokemon){
            console.log(`${targetPokemon.name} is fainted`);
        }

        this.takle = function(targetPokemon){
            let hpAfterTarget = targetPokemon.health - this.attack;
            
            console.log(`${targetPokemon.name}'s health is now reduced to ${hpAfterTarget}`);

            targetPokemon.health = hpAfterTarget;
            hpAfterTarget <= 0 ? this.faint(targetPokemon) : null;
        }
    }
}

//adding pokemons
let pikachu = new Pokemon('pikachu', 80);
let geodude = new Pokemon('godude', 30);
let charizard = new Pokemon('charizard', 50);
let bulbasaur = new Pokemon('bulbasaur', 90);

// stored all pokemon of Ash in the array variable
let ashPokemons = [pikachu, charizard, bulbasaur];


let hoennFriends = ['May', 'Max'];
let kantoFriends = ['Brock', 'Misty'];

// adding trainer
let trainerAsh = new Trainer('Ash ketchun', 21, ashPokemons, hoennFriends, kantoFriends);


//console.log trainerAsh
console.log(trainerAsh);


// console.log trainerAsh name
console.log('Result of dot notation:');
console.log(trainerAsh.name);


// console.log console.log trainersAsh pokemons
console.log('Result of square bracket notation:');
console.log(trainerAsh['pokemons']);


// console.log talk method of trainerAsh
console.log(`Result of the talk method:`);
trainerAsh.talk(pikachu);

// console.log all the pokemons object
console.log(pikachu);
console.log(geodude);
console.log(charizard);
console.log(bulbasaur);

// console.log pokemon used takle method to other pokemon. the argument is pokemon object
pikachu.takle(geodude);

console.log(geodude);